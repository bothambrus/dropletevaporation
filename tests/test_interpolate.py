#################################################################################
#   DropletEvaporation                                                          #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np
from .context import dropletevaporation
from dropletevaporation.interpolate import Interpolator


def test_interpolator():
    a = Interpolator([1, 2, 3], [1, 4, 9])

    assert np.abs(a.eval(1) - 1) < 1e-8
    assert np.abs(a.eval(1.5) - 2.5) < 1e-8
    assert np.abs(a.evalInv(1) - 1) < 1e-8
    assert np.abs(a.evalInv(6.5) - 2.5) < 1e-8
