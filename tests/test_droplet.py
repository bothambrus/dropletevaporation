#################################################################################
#   DropletEvaporation                                                          #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy
import numpy as np
from .context import dropletevaporation
from dropletevaporation.droplet import Droplet


def test_droplet_setT_setP():
    """
    Check properties of droplet.
    """
    WaterDrop = Droplet(volatileName="water", gasName="air")

    WaterDrop.set_P(101325.0)
    WaterDrop.set_T(300.0)

    assert np.abs(WaterDrop.volatile.Tsat - 373.1657873749236) < 1e-8
    assert np.abs(WaterDrop.volatile.hvap - -13419855.84033731) < 1e-8
    assert np.abs(WaterDrop.volatile.cpvap - 1864.8863260990001) < 1e-8
    assert np.abs(WaterDrop.volatile.rhop - 994.51147) < 1e-8
    assert np.abs(WaterDrop.volatile.cpp - 4182.9481) < 1e-8
    assert np.abs(WaterDrop.volatile.Psat - 3537.4483) < 1e-8
    assert np.abs(WaterDrop.volatile.Lv - 2431464.5) < 1e-8
    assert np.abs(WaterDrop.bathgas.h - 565.4416771489778) < 1e-8
    assert np.abs(WaterDrop.bathgas.cp - 1010.1625320679001) < 1e-8
    assert np.abs(WaterDrop.Xeq - 0.03491190032075006) < 1e-8
    assert np.abs(WaterDrop.Yeq - 0.022090881359807798) < 1e-8


def test_droplet_setd_setm():
    """
    Check size of droplet.
    """
    WaterDrop = Droplet(volatileName="water", gasName="air")

    WaterDrop.set_P(101325.0)
    WaterDrop.set_T(300.0)

    #
    # 1 mm water droplet
    #
    WaterDrop.set_d(1e-3)
    assert np.abs(WaterDrop.m - 5.207249880104643e-07) < 1e-16

    #
    # 1 g water droplet
    #
    WaterDrop.set_m(1e-3)
    assert np.abs(WaterDrop.d - 0.012429791977874987) < 1e-16


def test_droplet_wetBulb():
    """
    Check properties at wet bulb conditions.
    """
    WaterDrop = Droplet(volatileName="water", gasName="air")
    WaterDrop.set_P(101325.0)
    WaterDrop.set_T(300.0)
    WaterDrop.set_Yvapseen(0.0)
    WaterDrop.set_Tseen(300.0)

    WaterDrop.set_to_therm_wet_bulb()
    assert np.abs(WaterDrop.BMeq - 0.007237583637908632) < 1e-5
    assert np.abs(WaterDrop.T - 282.31602171331696) < 1e-5
    assert np.abs(WaterDrop.therm_wet_bulb_error) < 1e-8


def test_droplet_kinetic():
    """
    Check if velocity and Reynolds are calculated correctly
    """
    WaterDrop = Droplet(volatileName="water", gasName="air")
    WaterDrop.set_P(101325.0)
    WaterDrop.set_T(300.0)

    WaterDrop.set_d(1e-3)
    WaterDrop.refprops["nu"] = 1e-5

    WaterDrop.set_kinetic_model("constveloc", param=1.0)
    assert np.abs(WaterDrop.Re - 100.0) < 1e-5
    assert np.abs(WaterDrop.veloc - 1.0) < 1e-5

    WaterDrop.set_kinetic_model("constreynolds", param=200.0)
    assert np.abs(WaterDrop.Re - 200.0) < 1e-5
    assert np.abs(WaterDrop.veloc - 2.0) < 1e-5


def test_droplet_heat_and_mass_transfer():
    """
    Check Nusselt and Sherwood number calculation.
    """
    WaterDrop = Droplet(volatileName="water", gasName="air")
    WaterDrop.set_P(101325.0)
    WaterDrop.set_T(350.0)

    WaterDrop.set_d(1e-3)
    WaterDrop.refprops["nu"] = 1e-5

    WaterDrop.set_kinetic_model("constreynolds", param=200.0)
    assert np.abs(WaterDrop.Re - 200.0) < 1e-5
    assert np.abs(WaterDrop.veloc - 2.0) < 1e-5

    WaterDrop.refprops["Pr"] = 0.7
    WaterDrop.refprops["Sc"] = 2.0
    WaterDrop.refprops["Le"] = WaterDrop.refprops["Sc"] / WaterDrop.refprops["Pr"]
    WaterDrop.refprops["cp"] = 1000.0
    WaterDrop.refprops["cpvap"] = 1500.0
    WaterDrop.refprops["LK"] = 1e-4

    #
    # Basic model (rapid-mixing of Miller)
    #
    WaterDrop.set_evaporation_model("ranzmarshall", None, "equil")

    assert np.abs(WaterDrop.Nu0 - 9.534115288099276) < 1e-5
    assert np.abs(WaterDrop.Sh0 - 12.6907846176816) < 1e-5
    assert np.abs(WaterDrop.Nu - 9.534115288099276) < 1e-5
    assert np.abs(WaterDrop.Sh - 12.6907846176816) < 1e-5
    assert np.abs(WaterDrop.Phi_m - 0.6988233016858254) < 1e-5

    WaterDrop.set_Yvapseen(0.0)
    WaterDrop.__update_evaporation__()
    assert np.abs(WaterDrop.beta_eq - 0.2527621535754987) < 1e-5

    #
    # With Bird's correction
    #
    WaterDrop.set_evaporation_model("ranzmarshall", "birds", "equil")
    assert np.abs(WaterDrop.Nu0 - 9.534115288099276) < 1e-5
    assert np.abs(WaterDrop.Sh0 - 12.6907846176816) < 1e-5
    assert np.abs(WaterDrop.Nu - 8.37988975474975) < 1e-5
    assert np.abs(WaterDrop.Sh - 12.6907846176816) < 1e-5

    #
    # Langmuir-Knudsen without iteration with Bird's correction
    #
    WaterDrop.set_evaporation_model("ranzmarshall", "birds", "noneq1step")
    assert np.abs(WaterDrop.Xeq - 0.4110091191709845) < 1e-5
    assert np.abs(WaterDrop.Xneq - 0.36045668845588474) < 1e-5
    assert np.abs(WaterDrop.Yeq - 0.3035064889213612) < 1e-5
    assert np.abs(WaterDrop.Yneq - 0.26033218695188626) < 1e-5
    assert np.abs(WaterDrop.BMeq - 0.43576355571687914) < 1e-5
    assert np.abs(WaterDrop.BMneq - 0.3519582471475641) < 1e-5
    assert np.abs(WaterDrop.beta_neq - 0.21073302820456977) < 1e-5
    assert np.abs(WaterDrop.Nu0 - 9.534115288099276) < 1e-5
    assert np.abs(WaterDrop.Sh0 - 12.6907846176816) < 1e-5
    assert np.abs(WaterDrop.Nu - 8.564795616164675) < 1e-5
    assert np.abs(WaterDrop.Sh - 12.6907846176816) < 1e-5

    #
    # Iterative Langmuir-Knudsen
    #
    WaterDrop.set_evaporation_model("ranzmarshall", "birds", "noneq")
    assert np.abs(WaterDrop.Xeq - 0.4110091191709845) < 1e-5
    assert np.abs(WaterDrop.Xneq - 0.3677094174367134) < 1e-5
    assert np.abs(WaterDrop.Nu - 8.539265378165359) < 1e-5
    assert np.abs(WaterDrop.Sh - 12.6907846176816) < 1e-5

    #
    # Abramzon-Sirignano
    #
    WaterDrop.set_evaporation_model("ranzmarshall", "absir", "equil")

    assert np.abs(WaterDrop.Nu0 - 9.534115288099276) < 1e-5
    assert np.abs(WaterDrop.Sh0 - 12.6907846176816) < 1e-5
    assert np.abs(WaterDrop.Nu - 8.075624519731564) < 1e-5
    assert np.abs(WaterDrop.Sh - 11.999027645830564) < 1e-5

    #
    # Abramzon-Sirignano with non-equilibrium?
    # (Just to test)
    #
    WaterDrop.set_d(1e-6)
    WaterDrop.set_evaporation_model("ranzmarshall", "absir", "noneq")

    assert np.abs(WaterDrop.Nu0 - 9.534115288099276) < 1e-5
    assert np.abs(WaterDrop.Sh0 - 12.6907846176816) < 1e-5
    assert np.abs(WaterDrop.Nu - 9.525395709995145) < 1e-5
    assert np.abs(WaterDrop.Sh - 12.67308437952545) < 1e-5


def test_droplet_properties():
    """
    Check mean property calculation
    """
    WaterDrop = Droplet(volatileName="water", gasName="air")
    WaterDrop.set_P(101325.0)
    WaterDrop.set_T(350.0)
    WaterDrop.set_d(1e-3)
    WaterDrop.set_Yvapseen(0.0)
    WaterDrop.set_Tseen(600.0)
    #
    # Try without model:
    #
    WaterDrop.__update_mean_property__()
    #
    # With model:
    #
    WaterDrop.set_mean_property_model("cantera")

    assert np.abs(WaterDrop.refprops["rho"] - 0.7233373483846455) < 1e-5
    assert np.abs(WaterDrop.refprops["Df"] - 4.619828479554537e-05) < 1e-9
    assert np.abs(WaterDrop.refprops["mu"] - 2.171893449910705e-05) < 1e-9
    assert np.abs(WaterDrop.refprops["k"] - 0.03645835658480094) < 1e-6
    assert np.abs(WaterDrop.refprops["cp"] - 1207.2417338003931) < 1e-3
    assert np.abs(WaterDrop.refprops["Dt"] - 4.1750526646990684e-05) < 1e-9
    assert np.abs(WaterDrop.refprops["nu"] - 3.002601005963497e-05) < 1e-9
    assert np.abs(WaterDrop.refprops["Pr"] - 0.7191767977805664) < 1e-5
    assert np.abs(WaterDrop.refprops["Sc"] - 0.649937767008402) < 1e-5
    assert np.abs(WaterDrop.refprops["Le"] - 0.9037246043172678) < 1e-7
    assert np.abs(WaterDrop.refprops["cpvap"] - 1918.3171368542335) < 1e-7
    assert np.abs(WaterDrop.refprops["LK"] - 3.3225692761706393e-07) < 1e-12


def test_droplet_residual():
    """
    Check calculating the residual of mass and energy conservation
    """
    WaterDrop = Droplet(volatileName="water", gasName="air")
    WaterDrop.set_P(101325.0)
    WaterDrop.set_T(350.0)
    WaterDrop.set_d(1e-3)
    WaterDrop.set_Yvapseen(0.05)
    WaterDrop.set_Tseen(600.0)
    WaterDrop.set_mean_property_model("cantera")
    WaterDrop.set_kinetic_model("constreynolds", param=0.0)
    WaterDrop.set_evaporation_model("ranzmarshall", None, "equil")

    WaterDrop.residual()

    assert np.abs(WaterDrop.Nu - 2.0) < 1e-5
    assert np.abs(WaterDrop.Sh - 2.0) < 1e-5
    assert np.abs(WaterDrop.res[0] - -6.45968359994389e-08) < 1e-15
    assert np.abs(WaterDrop.res[1] - -43.012082702034505) < 1e-5
    assert np.abs(WaterDrop.evap_const - 8.387698510634214e-08) < 1e-8
    assert WaterDrop.residual_names[0] == "mass"
    assert WaterDrop.residual_names[1] == "temperature"
    assert np.abs(WaterDrop.get_mass_transfer_potential() - 0.3104035078881337) < 1e-5

    #
    # Change Reynolds
    #
    WaterDrop.set_kinetic_model("constreynolds", param=10.0)
    WaterDrop.residual()
    assert np.abs(WaterDrop.Re - 10.0) < 1e-5
    assert np.abs(WaterDrop.veloc - 0.3002091476044687) < 1e-5
    assert np.abs(WaterDrop.Nu - 3.700197567960662) < 1e-5
    assert np.abs(WaterDrop.Sh - 3.6434252733161503) < 1e-5
    assert np.abs(WaterDrop.res[0] - -1.176768724283071e-07) < 1e-15
    assert np.abs(WaterDrop.res[1] - -77.5984670529578) < 1e-5
    assert np.abs(WaterDrop.evap_const - 1.5279976369300462e-07) < 1e-8

    #
    # Change diameter
    #
    WaterDrop.set_d(0.5e-3)
    WaterDrop.residual()
    assert np.abs(WaterDrop.Re - 10.0) < 1e-5
    assert np.abs(WaterDrop.veloc - 0.6004182952089374) < 1e-5
    assert np.abs(WaterDrop.Nu - 3.700197567960662) < 1e-5
    assert np.abs(WaterDrop.Sh - 3.6434252733161503) < 1e-5
    assert np.abs(WaterDrop.res[0] - -5.883843621415355e-08) < 1e-15
    assert np.abs(WaterDrop.res[1] - -310.3938682118312) < 1e-5
    assert np.abs(WaterDrop.evap_const - 1.5279976369300462e-07) < 1e-8

    #
    # Change temperatures
    #
    WaterDrop.set_T(300)
    WaterDrop.set_Tseen(300)
    WaterDrop.residual()
    assert np.abs(WaterDrop.Re - 10.0) < 1e-5
    assert np.abs(WaterDrop.veloc - 0.3161571865617705) < 1e-5
    assert np.abs(WaterDrop.Nu - 3.6952022341864383) < 1e-5
    assert np.abs(WaterDrop.Sh - 3.6821382120569073) < 1e-5
    assert np.abs(WaterDrop.res[0] - 4.369930955226619e-09) < 1e-15
    assert np.abs(WaterDrop.res[1] - 39.57973148102069) < 1e-5
    assert np.abs(WaterDrop.evap_const - -1.1189350887942669e-08) < 1e-8

    #
    # Change seen vapur mass fraction
    #
    WaterDrop.set_Yvapseen(0.0)
    WaterDrop.residual()
    assert np.abs(WaterDrop.Re - 10.0) < 1e-5
    assert np.abs(WaterDrop.veloc - 0.31709506585556035) < 1e-5
    assert np.abs(WaterDrop.Nu - 3.6940859690280243) < 1e-5
    assert np.abs(WaterDrop.Sh - 3.6838017372469256) < 1e-5
    assert np.abs(WaterDrop.res[0] - -3.4064188653731773e-09) < 1e-15
    assert np.abs(WaterDrop.res[1] - -30.852923166233786) < 1e-5
    assert np.abs(WaterDrop.evap_const - 8.722246723459135e-09) < 1e-8

    #
    # Change diameter
    #
    WaterDrop.set_kinetic_model("constreynolds", param=0.0)
    WaterDrop.set_d(1e-6)
    WaterDrop.residual()
    assert np.abs(WaterDrop.Re - 0.0) < 1e-5
    assert np.abs(WaterDrop.Nu - 2.0) < 1e-5
    assert np.abs(WaterDrop.Sh - 2.0) < 1e-5
    assert np.abs(WaterDrop.res[0] - -3.6988080340273146e-12) < 1e-15
    assert np.abs(WaterDrop.res[1] - -4128942.4597785817) < 1e-5
    assert np.abs(WaterDrop.evap_const - 4.735459368113371e-09) < 1e-8

    #
    # Change evaporation model to Langmuir-Knudsen
    #
    WaterDrop.set_evaporation_model("ranzmarshall", "birds", "noneq")
    WaterDrop.residual()
    assert np.abs(WaterDrop.Nu - 1.9739479067948644) < 1e-5
    assert np.abs(WaterDrop.Sh - 2.0) < 1e-5
    assert np.abs(WaterDrop.Yeq - 0.022090881359807798) < 1e-5
    assert np.abs(WaterDrop.Yneq - 0.013990369321343461) < 1e-5
    assert np.abs(WaterDrop.BMeq - 0.02258991243534546) < 1e-5
    assert np.abs(WaterDrop.BMneq - 0.01418887695013089) < 1e-5
    assert np.abs(WaterDrop.res[0] - -2.3328780428519984e-12) < 1e-15
    assert np.abs(WaterDrop.res[1] - -2604168.4553520796) < 1e-5
    assert np.abs(WaterDrop.evap_const - 2.9867051982854817e-09) < 1e-8
    assert np.abs(WaterDrop.get_mass_transfer_potential() - 0.014089157004629977) < 1e-5

    #
    # Change to non-equilibrium dilute
    #
    WaterDrop.set_evaporation_model("ranzmarshall", None, "noneq", "dilute")
    WaterDrop.residual()
    assert np.abs(WaterDrop.get_mass_transfer_potential() - 0.013990369321343461) < 1e-5

    #
    # Change to dilute
    #
    WaterDrop.set_evaporation_model("ranzmarshall", None, "equil", "dilute")
    WaterDrop.set_BMeq(22.0)
    WaterDrop.set_Yvapseen(0.05)
    WaterDrop.residual()
    assert np.abs(WaterDrop.get_mass_transfer_potential() - 0.9065217391304335) < 1e-5
    assert np.abs(WaterDrop.Yeq - 0.9565217391304335) < 1e-5


def test_droplet_jacobian():
    """
    Check calculating the jacobian of mass and energy conservation
    """
    WaterDrop = Droplet(volatileName="water", gasName="air")
    WaterDrop.set_P(101325.0)
    WaterDrop.set_T(350.0)
    WaterDrop.set_d(1e-3)
    WaterDrop.set_Yvapseen(0.05)
    WaterDrop.set_Tseen(600.0)
    WaterDrop.set_mean_property_model("cantera")
    WaterDrop.set_kinetic_model("constreynolds", param=0.0)
    WaterDrop.set_evaporation_model("ranzmarshall", None, "equil")

    WaterDrop.jacobian()

    assert np.abs(WaterDrop.d - 1e-3) < 1e-8
    assert np.abs(WaterDrop.T - 350) < 1e-5
    assert np.abs(WaterDrop.Nu - 2.0) < 1e-5
    assert np.abs(WaterDrop.Sh - 2.0) < 1e-5
    assert np.abs(WaterDrop.res[0] - -6.45968359994389e-08) < 1e-12
    assert np.abs(WaterDrop.res[1] - -43.012082702034505) < 1e-3

    #
    # d \dot{m} / d m
    #
    assert np.abs(WaterDrop.jac[0, 0] - -0.04192452081691708) < 1e-8
    #
    # d \dot{T} / d m
    #
    assert np.abs(WaterDrop.jac[1, 0] - 55803353.180410884) < 1e-1
    #
    # d \dot{m} / d T
    #
    assert np.abs(WaterDrop.jac[0, 1] - -4.1118649387101764e-09) < 1e-15
    #
    # d \dot{T} / d T
    #
    assert np.abs(WaterDrop.jac[1, 1] - -4.379986923540002) < 1e-6

    #
    # Double check mass:
    #
    dm = copy.deepcopy(WaterDrop.m * 0.01)
    WaterDrop.set_m(WaterDrop.m + dm)
    WaterDrop.jacobian()
    assert (
        np.abs(WaterDrop.res[0] - (-6.45968359994389e-08 + (-0.04192452081691708 * dm)))
        < 1e-12
    )
    assert (
        np.abs(WaterDrop.res[1] - (-43.012082702034505 + (55803353.180410884 * dm)))
        < 1e-2
    )

    #
    # Double check temperature:
    #
    WaterDrop.set_m(WaterDrop.m - dm)
    dT = -1 * copy.deepcopy(WaterDrop.T * 0.001)
    WaterDrop.set_T(WaterDrop.T + dT)
    WaterDrop.jacobian()
    assert (
        np.abs(
            WaterDrop.res[0] - (-6.45968359994389e-08 + (-4.1118649387101764e-09 * dT))
        )
        < 1e-11
    )
    assert (
        np.abs(WaterDrop.res[1] - (-43.012082702034505 + (-4.379986923540002 * dT)))
        < 1e-2
    )

    #
    # Check the two together
    #
    WaterDrop.set_m(WaterDrop.m + dm)
    WaterDrop.jacobian()
    assert (
        np.abs(
            WaterDrop.res[0]
            - (
                -6.45968359994389e-08
                + (-0.04192452081691708 * dm)
                + (-4.1118649387101764e-09 * dT)
            )
        )
        < 1e-11
    )
    assert (
        np.abs(
            WaterDrop.res[1]
            - (
                -43.012082702034505
                + (55803353.180410884 * dm)
                + (-4.379986923540002 * dT)
            )
        )
        < 2e-2
    )


def test_droplet_advance():
    """
    Check advancing the droplet in time.
    """
    WaterDrop = Droplet(volatileName="water", gasName="air")
    WaterDrop.set_P(101325.0)
    WaterDrop.set_T(350.0)
    WaterDrop.set_d(1e-3)
    WaterDrop.set_Yvapseen(0.05)
    WaterDrop.set_Tseen(600.0)
    WaterDrop.set_mean_property_model("cantera")
    WaterDrop.set_kinetic_model("constreynolds", param=0.0)
    WaterDrop.set_evaporation_model("ranzmarshall", None, "equil")
    WaterDrop.set_temporal_scheme("implicit")

    #
    # Check implicit
    #
    WaterDrop.advance(5.0)

    assert np.abs(WaterDrop.d - 0.0009057837592347465) < 1e-8
    assert np.abs(WaterDrop.T - 336.06383174504765) < 1e-5

    #
    # Check explicit
    #
    WaterDrop.set_T(350.0)
    WaterDrop.set_d(1e-3)
    WaterDrop.set_temporal_scheme("explicit")

    WaterDrop.advance(5.0)

    assert np.abs(WaterDrop.d - 0.0009057853289166043) < 1e-8
    assert np.abs(WaterDrop.T - 336.06384018834416) < 1e-5

    #
    # Check single step
    #
    WaterDrop.set_T(350.0)
    WaterDrop.set_d(1e-3)
    WaterDrop.set_temporal_scheme("implicit")

    WaterDrop.advance(5.0, substeps=False)

    assert np.abs(WaterDrop.d - 0.0009375568091258801) < 1e-8
    assert np.abs(WaterDrop.T - 339.5247149495318) < 1e-5

    #
    # Check time step estimation for "maxed-out" dilute models
    #
    WaterDrop.set_evaporation_model("ranzmarshall", None, "equil", "dilute")
    WaterDrop.set_BMeq(22.0)
    WaterDrop.set_Yvapseen(0.05)
    delta_t, nt = WaterDrop.advance()
    assert np.abs(WaterDrop.d - 0.0009350977023667609) < 1e-8
    assert np.abs(WaterDrop.T - 365.50702072955556) < 1e-5
    assert np.abs(delta_t - 0.03565959312259129) < 1e-8


def test_droplet_wet_bulb():
    """
    Check setting to the wet bulb state.
    """
    WaterDrop = Droplet(volatileName="water", gasName="air")
    WaterDrop.set_P(101325.0)
    WaterDrop.set_T(350.0)
    WaterDrop.set_d(1e-3)
    WaterDrop.set_Yvapseen(0.05)
    WaterDrop.set_Tseen(600.0)
    WaterDrop.set_mean_property_model("cantera")
    WaterDrop.set_kinetic_model("constreynolds", param=0.0)
    WaterDrop.set_evaporation_model("ranzmarshall", None, "equil")
    WaterDrop.set_temporal_scheme("implicit")

    assert np.abs(WaterDrop.m - 5.134251937925586e-07) < 1e-8
    #
    # Test implicit
    #
    WaterDrop.set_to_psychro_wet_bulb()

    assert np.abs(WaterDrop.m - 5.134251937925586e-07) < 1e-8
    assert np.abs(WaterDrop.d - 0.0009985965173543335) < 1e-8
    assert np.abs(WaterDrop.T - 335.81868048002065) < 1e-3

    #
    # Test Something more extreme
    #
    DDDrop = Droplet(volatileName="n-dodecane", gasName="air")
    DDDrop.set_P(101325.0)
    DDDrop.set_T(DDDrop.volatile.Tsat - 1)
    DDDrop.set_d(1e-3)
    DDDrop.set_Yvapseen(0.0)
    DDDrop.set_Tseen(1800.0)
    DDDrop.set_mean_property_model("cantera")
    DDDrop.set_kinetic_model("constreynolds", param=0.0)
    DDDrop.set_evaporation_model("ranzmarshall", None, "equil")
    DDDrop.set_temporal_scheme("implicit")

    DDDrop.set_to_psychro_wet_bulb()
    assert np.abs(DDDrop.T - 489.5067210324153) < 1e-3
    assert np.abs(DDDrop.psych_wet_bulb_steps - 90) < 1e-3
    assert np.abs(DDDrop.psych_wet_bulb_error - 0.1366818500666927) < 1e-3
    assert np.abs(DDDrop.BMeq - 30769.45577045505) < 1e-3

    #
    # Test supercritical
    #
    DDDrop.set_P(60.0e5)
    DDDrop.set_Tseen(672.0)
    DDDrop.set_T(DDDrop.volatile.Tsat - 1)

    DDDrop.set_to_psychro_wet_bulb()
    assert np.abs(DDDrop.T - 631.2124833649079) < 1e-3
    assert np.abs(DDDrop.psych_wet_bulb_steps - 1022) < 1e-3
    assert np.abs(DDDrop.psych_wet_bulb_error - -4.360085138461626e-05) < 1e-7
    assert np.abs(DDDrop.BMeq - 1.5920097356750416) < 1e-3
    assert np.abs(DDDrop.Yeq - 0.6141989799511427) < 1e-3
    assert np.abs(DDDrop.Yeq_max - 0.7203150420801777) < 1e-3
    assert np.abs(DDDrop.BMeq_max - 2.5754514916982827) < 1e-3

    #
    # Test NaN
    #
    DDDrop.set_Tseen(700.0)
    DDDrop.set_to_psychro_wet_bulb()
    assert np.isnan(DDDrop.T)

    DDDrop.set_Tseen(675.0)
    DDDrop.set_to_psychro_wet_bulb(reject_high_error=False)
    assert np.abs(DDDrop.T - 658.0) < 1e-3
    assert np.abs(DDDrop.psych_wet_bulb_steps - 24) < 1e-3
    assert np.abs(DDDrop.psych_wet_bulb_error - 4.052731172290174) < 1e-7
