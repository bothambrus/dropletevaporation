#################################################################################
#   DropletEvaporation                                                          #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np
from .context import dropletevaporation
from dropletevaporation.interface import PhaseChange


def test_interface_setT_setP():
    """
    Check properties of interface.
    """
    WaterAir = PhaseChange(volatileName="water", gasName="air")

    WaterAir.set_P(101325.0)
    WaterAir.set_T(300.0)

    assert np.abs(WaterAir.volatile.Tsat - 373.1657873749236) < 1e-8
    assert np.abs(WaterAir.volatile.hvap - -13419855.84033731) < 1e-8
    assert np.abs(WaterAir.volatile.cpvap - 1864.8863260990001) < 1e-8
    assert np.abs(WaterAir.volatile.rhop - 994.51147) < 1e-8
    assert np.abs(WaterAir.volatile.cpp - 4182.9481) < 1e-8
    assert np.abs(WaterAir.volatile.Psat - 3537.4483) < 1e-8
    assert np.abs(WaterAir.volatile.Lv - 2431464.5) < 1e-8
    assert np.abs(WaterAir.bathgas.h - 565.4416771489778) < 1e-8
    assert np.abs(WaterAir.bathgas.cp - 1010.1625320679001) < 1e-8
    assert np.abs(WaterAir.Xeq - 0.03491190032075006) < 1e-8
    assert np.abs(WaterAir.Yeq - 0.022090881359807798) < 1e-8


def test_interface_setYseen():
    """
    Check properties phase change.
    """
    WaterAir = PhaseChange(volatileName="water", gasName="air")
    WaterAir.set_P(101325.0)
    WaterAir.set_T(350.0)
    WaterAir.set_Yvapseen(0.0)
    assert np.abs(WaterAir.BMeq - 0.43576355571687914) < 1e-8

    WaterAir.set_T(373.15)
    assert np.abs(WaterAir.BMeq - 1097.7949992815645) < 1e-8

    WaterAir.set_BMeq(10)
    assert np.abs(WaterAir.BMeq - 10) < 1e-8
    assert np.abs(WaterAir.Xeq - 0.9412238745587311) < 1e-8
    assert np.abs(WaterAir.Yeq - 0.9090909090909102) < 1e-8
    assert np.abs(WaterAir.T - 371.47417756830504) < 1e-8
    assert np.abs(WaterAir.volatile.Tsat - 373.1657873749236) < 1e-8
    assert np.abs(WaterAir.volatile.hvap - -13285741.733895248) < 1e-8
    assert np.abs(WaterAir.volatile.cpvap - 1889.6368131159159) < 1e-8
    assert np.abs(WaterAir.volatile.rhop - 973.977020546361) < 1e-8
    assert np.abs(WaterAir.volatile.cpp - 4217.233175886411) < 1e-8
    assert np.abs(WaterAir.volatile.Psat - 95369.50908966344) < 1e-8
    assert np.abs(WaterAir.volatile.Lv - 2269077.2045055544) < 1e-8
    assert np.abs(WaterAir.bathgas.h - 73051.69288937113) < 1e-8
    assert np.abs(WaterAir.bathgas.cp - 1018.4180394931849) < 1e-8

    WaterAir.set_Xeq(0.5)
    assert np.abs(WaterAir.BMeq - 0.6244648805665436) < 1e-8
    assert np.abs(WaterAir.volatile.Psat - 50662.5) < 1e-8

    WaterAir.set_Yeq(0.5)
    WaterAir.set_Yvapseen(0.1)
    assert np.abs(WaterAir.BMeq - 0.8) < 1e-8
    assert np.abs(WaterAir.volatile.Psat - 62374.38630539208) < 1e-8
    assert np.abs(WaterAir.seen.W - 27.21223489229709) < 1e-8
    assert np.abs(WaterAir.seen.X["H2O"] - 0.1510532050640971) < 1e-8

    WaterAir.set_Tseen(300)
    assert np.abs(WaterAir.seen.T - 300.0) < 1e-8
    assert np.abs(WaterAir.seen.h - -1341476.6865242973) < 1e-8


def test_interface_wetBulb():
    """
    Check properties at wet bulb conditions.
    """
    DodecaneAir = PhaseChange(volatileName="n-dodecane", gasName="air")
    DodecaneAir.set_P(101325.0)
    DodecaneAir.set_T(273.0)
    DodecaneAir.set_Yvapseen(0.8)
    DodecaneAir.set_Tseen(2000.0)

    DodecaneAir.set_to_therm_wet_bulb()
    assert np.abs(DodecaneAir.BMeq - 19.45773885753793) < 1e-5

    DodecaneAir.set_P(6000000.0)
    DodecaneAir.set_Yvapseen(0.0)
    DodecaneAir.set_Tseen(2000.0)

    DodecaneAir.set_to_therm_wet_bulb()
    assert np.isnan(DodecaneAir.BMeq)
    assert np.isnan(DodecaneAir.T)

    DodecaneAir.set_P(6000000.0)
    DodecaneAir.set_Yvapseen(0.0)
    DodecaneAir.set_Tseen(600.0)

    DodecaneAir.set_to_therm_wet_bulb()
    assert np.abs(DodecaneAir.BMeq - 0.2893413422472831) < 1e-5
    assert np.abs(DodecaneAir.T - 537.299579116818) < 1e-5
    assert np.abs(DodecaneAir.therm_wet_bulb_error) < 1e-7
