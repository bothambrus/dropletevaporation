#################################################################################
#   DropletEvaporation                                                          #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np
from .context import dropletevaporation
from dropletevaporation.property import SimpleGas
from dropletevaporation.nasapoly import T2hcp, h2Tcp


def test_T2H():
    """
    Test calculating enthalpy and specific heat from teperature
    """
    Air = SimpleGas(name="air")

    h, cp = T2hcp(Air.cpcoe, 300.0)
    assert np.abs(h - 565.4416771489778) < 1e-8
    assert np.abs(cp - 1010.1625320679001) < 1e-8

    h, cp = T2hcp(Air.cpcoe, 700.0)
    assert np.abs(h - 416733.06023116765) < 1e-8
    assert np.abs(cp - 1080.4708277759) < 1e-8

    h, cp = T2hcp(Air.cpcoe, 1200.0)
    assert np.abs(h - 985389.5803437773) < 1e-8
    assert np.abs(cp - 1182.624531035072) < 1e-8


def test_H2T():
    """
    Test calculating temperature and specific heat from enthalpy
    """
    Air = SimpleGas(name="air")

    T, cp = h2Tcp(Air.cpcoe, 565.4416771489778)
    assert np.abs(T - 300.0) < 1e-8
    assert np.abs(cp - 1010.1625320679001) < 1e-8

    T, cp = h2Tcp(Air.cpcoe, 416733.06023116765)
    assert np.abs(T - 700.0) < 1e-8
    assert np.abs(cp - 1080.4708277759) < 1e-8

    T, cp = h2Tcp(Air.cpcoe, 985389.5803437773)
    assert np.abs(T - 1200.0) < 1e-8
    assert np.abs(cp - 1182.624531035072) < 1e-8
