#################################################################################
#   DropletEvaporation                                                          #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import cantera as ct
import numpy as np
from .context import dropletevaporation
from dropletevaporation.property import SimpleGas, Volatile, TwoPhaseSystem


def test_bath_gas():
    """
    Check bath gas properties.
    """
    Air = SimpleGas(name="air")
    assert np.abs(Air.W - 28.8487) < 1e-8
    assert (
        np.all(
            np.abs(
                np.transpose(Air.cpcoe)
                - np.array(
                    [
                        [
                            9.80027581e02,
                            1.39145377e-01,
                            3.06020284e-04,
                            6.98175695e-07,
                            3.60195091e-10,
                            2.98188943e05,
                        ],
                        [
                            8.65027691e02,
                            4.28585162e-01,
                            1.75288492e-04,
                            3.56573120e-08,
                            2.84807148e-12,
                            2.77326162e05,
                        ],
                    ]
                )
            )
        )
        < 1e-8
    )
    assert np.abs(Air.X["O2"] - 0.21) < 1e-8
    assert np.abs(Air.X["N2"] - 0.79) < 1e-8

    Air.set_T(300.0)
    assert np.abs(Air.h - 565.4416771489778) < 1e-8
    assert np.abs(Air.cp - 1010.1625320679001) < 1e-8

    Air.set_h(416733.06023116765)
    assert np.abs(Air.T - 700.0) < 1e-8
    assert np.abs(Air.cp - 1080.4708277759) < 1e-8

    N2 = SimpleGas(name="N2")
    assert np.abs(N2.W - 28.0140005) < 1e-8
    assert (
        np.all(
            np.abs(
                np.transpose(N2.cpcoe)
                - np.array(
                    [
                        [
                            9.79036438e02,
                            4.17960972e-01,
                            -1.17627124e-03,
                            1.67438304e-06,
                            -7.25624894e-10,
                            -3.02999750e05,
                        ],
                        [
                            8.68617065e02,
                            4.41626668e-01,
                            -1.68721832e-04,
                            2.99676799e-08,
                            -2.00437236e-12,
                            -2.73883312e05,
                        ],
                    ]
                )
            )
        )
        < 1e-8
    )
    assert np.abs(N2.X["N2"] - 1.0) < 1e-8

    N2.set_T(300.0)
    assert np.abs(N2.h - 1970.9559375160025) < 1e-8
    assert np.abs(N2.cp - 1037.8910984386) < 1e-8

    N2.set_h(416733.06023116765)
    assert np.abs(N2.T - 691.2090926939866) < 1e-8
    assert np.abs(N2.cp - 1093.2601061702305) < 1e-8


def test_volatile_setT():
    """
    Check volatile temperature dependent properties.
    """

    Water = Volatile(name="water")
    Hep = Volatile(name="n-heptane")
    Deca = Volatile(name="n-decane")
    Dode = Volatile(name="n-dodecane")
    OME1 = Volatile(name="OME1")

    Water.set_T(300.0)
    Hep.set_T(300.0)
    Deca.set_T(300.0)
    Dode.set_T(300.0)
    OME1.set_T(300.0)

    assert np.abs(Water.hvap - -13419855.84033731) < 1e-8
    assert np.abs(Water.cpvap - 1864.8863260990001) < 1e-8
    assert np.abs(Water.rhop - 994.51147) < 1e-8
    assert np.abs(Water.cpp - 4182.9481) < 1e-8
    assert np.abs(Water.Psat - 3537.4483) < 1e-8
    assert np.abs(Water.Lv - 2431464.5) < 1e-8

    assert np.abs(Hep.hvap - -1878174.5856361997) < 1e-8
    assert np.abs(Hep.cpvap - 1664.2459911025003) < 1e-8
    assert np.abs(Hep.rhop - 679.9807) < 1e-8
    assert np.abs(Hep.cpp - 2245.8746) < 1e-8
    assert np.abs(Hep.Psat - 6647.832) < 1e-8
    assert np.abs(Hep.Lv - 364464.54) < 1e-8

    # TPS = TwoPhaseSystem(volatileName="n-decane", gasName="air")
    # TPS.set_P(101325.0)
    # TPS.set_T(300.0)
    # id = TPS.gasobject.species_index(TPS.volatile.formula)
    # cps = TPS.gasobject.species()[id].thermo.coeffs * ct.gas_constant / TPS.volatile.W
    # print(cps[8:14])
    # print(cps[1:7])

    assert np.abs(Deca.hvap - -1759499.042372658) < 1e-8
    assert np.abs(Deca.cpvap - 1656.3889409782) < 1e-8
    assert np.abs(Deca.rhop - 725.21967) < 1e-8
    assert np.abs(Deca.cpp - 2220.0513) < 1e-8
    assert np.abs(Deca.Psat - 205.64029) < 1e-8
    assert np.abs(Deca.Lv - 356693.47) < 1e-8

    assert np.abs(Dode.hvap - -1709158.965338472) < 1e-8
    assert np.abs(Dode.cpvap - 1652.3236011037998) < 1e-8
    assert np.abs(Dode.rhop - 743.83055) < 1e-8
    assert np.abs(Dode.cpp - 2211.6909) < 1e-8
    assert np.abs(Dode.Psat - 20.83) < 1e-8
    assert np.abs(Dode.Lv - 354436.51) < 1e-8

    assert np.abs(OME1.hvap - -4515685.606703719) < 1e-8
    assert np.abs(OME1.cpvap - 1409.010594588) < 1e-8
    assert np.abs(OME1.rhop - 774.0393) < 1e-8
    assert np.abs(OME1.cpp - 2017.12) < 1e-8
    assert np.abs(OME1.Psat - 60217.235) < 1e-8
    assert np.abs(OME1.Lv - 362553.0) < 1e-8


def test_volatile_setP():
    """
    Check volatile pressure dependent properties.
    """

    Water = Volatile(name="water")
    Hep = Volatile(name="n-heptane")
    Dode = Volatile(name="n-dodecane")

    Water.set_P(101325.0)
    Hep.set_P(101325.0)
    Dode.set_P(101325.0)

    assert np.abs(Water.Tsat - 373.1657873749236) < 1e-8
    assert np.abs(Hep.Tsat - 371.54603471149915) < 1e-8
    assert np.abs(Dode.Tsat - 489.5150072629176) < 1e-8


def test_TwoPhaseSys_setT_setP():
    """
    Check properties of individual components of two phase system.
    """
    WetAir = TwoPhaseSystem(volatileName="water", gasName="air")

    WetAir.set_P(101325.0)
    WetAir.set_T(300.0)

    assert np.abs(WetAir.volatile.Tsat - 373.1657873749236) < 1e-8
    assert np.abs(WetAir.volatile.hvap - -13419855.84033731) < 1e-8
    assert np.abs(WetAir.volatile.cpvap - 1864.8863260990001) < 1e-8
    assert np.abs(WetAir.volatile.rhop - 994.51147) < 1e-8
    assert np.abs(WetAir.volatile.cpp - 4182.9481) < 1e-8
    assert np.abs(WetAir.volatile.Psat - 3537.4483) < 1e-8
    assert np.abs(WetAir.volatile.Lv - 2431464.5) < 1e-8
    assert np.abs(WetAir.bathgas.h - 565.4416771489778) < 1e-8
    assert np.abs(WetAir.bathgas.cp - 1010.1625320679001) < 1e-8
    assert np.abs(WetAir.Xeq - 0.03491190032075006) < 1e-8
    assert np.abs(WetAir.Yeq - 0.022090881359807798) < 1e-8

    WetAir.set_P(4000.0)
    assert np.abs(WetAir.Xeq - 0.884362075) < 1e-8
    assert np.abs(WetAir.Yeq - 0.8268610775971218) < 1e-8


def test_TwoPhaseSys_setXeq_setYeq():
    """
    Check setting equilibrium vapour mole and mass fraction.
    """
    VapourAir = TwoPhaseSystem(volatileName="n-heptane", gasName="air")

    VapourAir.set_P(101325.0)
    VapourAir.set_Xeq(0.5)
    assert np.abs(VapourAir.volatile.Tsat - 371.54603471149915) < 1e-8
    assert np.abs(VapourAir.volatile.hvap - -1789746.3432768742) < 1e-8
    assert np.abs(VapourAir.volatile.cpvap - 1895.7002682353027) < 1e-8
    assert np.abs(VapourAir.volatile.rhop - 635.7656149873279) < 1e-8
    assert np.abs(VapourAir.volatile.cpp - 2453.021851973176) < 1e-8
    assert np.abs(VapourAir.volatile.Psat - 50662.49999999996) < 1e-8
    assert np.abs(VapourAir.volatile.Lv - 333155.4334321854) < 1e-8
    assert np.abs(VapourAir.bathgas.h - 50848.57633498404) < 1e-8
    assert np.abs(VapourAir.bathgas.cp - 1015.7274152609325) < 1e-8
    assert np.abs(VapourAir.Xeq - 0.5) < 1e-8
    assert np.abs(VapourAir.Yeq - 0.7764613153717392) < 1e-8
    assert np.abs(VapourAir.T - 349.6434789701521) < 1e-8

    VapourAir.set_Yeq(0.5)
    assert np.abs(VapourAir.volatile.Tsat - 371.54603471149915) < 1e-8
    assert np.abs(VapourAir.volatile.hvap - -1830173.9266398652) < 1e-8
    assert np.abs(VapourAir.volatile.cpvap - 1795.496765674307) < 1e-8
    assert np.abs(VapourAir.volatile.rhop - 655.815580681862) < 1e-8
    assert np.abs(VapourAir.volatile.cpp - 2355.808964838499) < 1e-8
    assert np.abs(VapourAir.volatile.Psat - 22650.057219958464) < 1e-8
    assert np.abs(VapourAir.volatile.Lv - 347518.3262593693) < 1e-8
    assert np.abs(VapourAir.bathgas.h - 28630.32877564471) < 1e-8
    assert np.abs(VapourAir.bathgas.cp - 1013.1831111654599) < 1e-8
    assert np.abs(VapourAir.Xeq - 0.22353868462826018) < 1e-8
    assert np.abs(VapourAir.Yeq - 0.5) < 1e-8
    assert np.abs(VapourAir.T - 327.74155731574075) < 1e-8
