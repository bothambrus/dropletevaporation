import os
import sys
import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
from dropletevaporation.interface import PhaseChange
from dropletevaporation.property import y2x


P = 101325.0
P = 60e5
# P = 28e5

liqs = ["water", "n-heptane", "n-dodecane"]
# liqs = ["n-heptane"]
# liqs = ["n-dodecane"]

for liq in liqs:
    drop = PhaseChange(volatileName=liq, gasName="air")
    drop.set_P(P)
    drop.set_T(300.0)
    print(liq)
    print("P_crit={} bar".format(drop.volatile.Pc / 1e5))
    drop.set_Yvapseen(0.4)
    print("X_max={}".format(drop.Xeq_max))
    print("Y_max={}".format(drop.Yeq_max))
    print("BM_max,drop={}".format(drop.BMeq_max))
    drop.set_T(300.0)

    fig = plt.figure()
    ax = []
    ax.append(fig.add_subplot(4, 1, 1))
    ax.append(fig.add_subplot(4, 1, 2))
    ax.append(fig.add_subplot(4, 1, 3))
    ax.append(fig.add_subplot(4, 1, 4))

    #
    # Mixture inlet fractions:
    #
    Ysvec = [0, 0.2, 0.4, 0.6, 0.8]
    # Ysvec = [0.4]

    npoin = 2000

    Twb = np.empty([npoin])
    Yfwb = np.empty([npoin])
    BM = np.empty([npoin])
    err = np.empty([npoin])

    for Ys in Ysvec:
        #
        # Set seen vapur mass fraction
        #
        drop.set_Yvapseen(Ys)

        #
        # Start temperature from spot where relative humidity is 100%
        #
        Xs = y2x(Ys, drop.volatile.W, drop.bathgas.W)
        if Ys > 1e-6:
            Tmin = min(drop.volatile.Tc, drop.volatile.Tp2Psat.evalInv(P * Xs))
        else:
            Tmin = 288.15
        print("Ys={}, Tmin={}".format(Ys, Tmin))
        T = np.linspace(Tmin, 2000, npoin)
        for i, Ti in enumerate(T):
            drop.set_Tseen(Ti)
            drop.set_to_therm_wet_bulb()

            Twb[i] = drop.T
            Yfwb[i] = drop.Yeq
            BM[i] = drop.BMeq
            err[i] = drop.therm_wet_bulb_error

        ax[0].plot(T, Twb, label="$Y_{{f,s}}={}$".format(Ys))
        ax[1].plot(T, Yfwb)
        # ax[2].plot(T, BM)
        ax[2].plot(T, BM, ".", markersize=2)
        ax[3].plot(T, err)

    ax[0].legend(
        loc="lower right",
        prop={"size": 12},
        framealpha=0.0,
        frameon=False,
        shadow=False,
        ncol=3,
    )  # , bbox_to_anchor=(1, 1.2))   #
    ax[0].set_xlabel("$T_s$")
    ax[1].set_xlabel("$T_s$")
    ax[2].set_xlabel("$T_s$")
    ax[3].set_xlabel("$T_s$")
    ax[0].set_ylabel("$T_{wb}^{th}$")
    ax[1].set_ylabel("$Y_{f,wb}^{th}$")
    ax[2].set_ylabel("$B_M$")
    ax[3].set_ylabel("$err$")

    plt.pause(0.1)

plt.show()
