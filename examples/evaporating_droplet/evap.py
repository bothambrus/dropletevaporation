import os
import sys
import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
from dropletevaporation.droplet import Droplet


d = 10e-6

#
# Get figure
#
fig = plt.figure()
ax = []
ax.append(fig.add_subplot(3, 1, 1))
ax.append(fig.add_subplot(3, 1, 2))
ax.append(fig.add_subplot(3, 1, 3))


#
# Set up Droplet object
#
drop = Droplet(volatileName="n-heptane", gasName="air")
for model in ["rapid-mixing", "Birds", "Abramzon-Sirignano", "Langmuir-Knudsen"]:
    #
    # Set conditions
    #
    drop.set_P(101325.0)
    drop.set_T(288.15)
    drop.set_Yvapseen(0.0)
    drop.set_Tseen(1000.0)
    print("Tsat:", drop.volatile.Tsat, model)
    drop.set_d(d)
    #
    # Set models
    #
    drop.set_mean_property_model("cantera")
    drop.set_kinetic_model("constreynolds", param=0.0)
    if model == "rapid-mixing":
        drop.set_evaporation_model("ranzmarshall", None, "equil")
    elif model == "Birds":
        drop.set_evaporation_model("ranzmarshall", "birds", "equil")
    elif model == "Abramzon-Sirignano":
        drop.set_evaporation_model("ranzmarshall", "absir", "equil")
    elif model == "Langmuir-Knudsen":
        drop.set_evaporation_model("ranzmarshall", "birds", "noneq")
    drop.set_temporal_scheme("implicit")

    time = []
    dt = []
    dia = []
    m = []
    T = []
    BM = []

    #
    # Save initial condition
    #
    time.append(0)
    dt.append(0)
    dia.append(drop.d)
    m.append(drop.m)
    T.append(drop.T)
    BM.append(drop.BMeq)

    while drop.m / m[0] > 0.0005:
        #
        # Advance droplet until it completely evaporates
        # using an automatic time step estimation
        #
        delta_t, nt = drop.advance()

        #
        # Save state
        #
        time.append(time[-1] + delta_t)
        dt.append(delta_t)
        dia.append(drop.d)
        m.append(drop.m)
        T.append(drop.T)
        BM.append(drop.BMeq)

    ax[0].plot(time, np.array(dia) ** 2, label=model)
    ax[1].plot(time, T)
    ax[2].plot(time, BM)

    ax[0].legend(
        loc="upper right",
        prop={"size": 12},
        framealpha=0.0,
        frameon=False,
        shadow=False,
        ncol=1,
    )  # , bbox_to_anchor=(1, 1.2))   #
    ax[-1].set_xlabel("$t \ [s]$")
    ax[0].set_ylabel("$d^2 \ [m^2]$")
    ax[1].set_ylabel("$T \ [K]$")
    ax[2].set_ylabel("$B_M^{eq}\ [-]$")

    plt.pause(0.001)


plt.show()
