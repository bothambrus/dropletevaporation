import os
import sys
import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
from dropletevaporation.droplet import Droplet
from dropletevaporation.property import y2x


P = 101325.0
# P = 60.0e5
# liqs = ["water", "n-heptane", "n-dodecane", "OME1"]
# liqs = ["n-dodecane"]
# liqs = ["n-heptane"]
# liqs = ["water"]
liqs = ["OME1"]

# model = "diffusion-only"
# model = "rapid-mixing"
model = "Birds"
# model = "Abramzon-Sirignano"
# model = "Langmuir-Knudsen"

d = 1e-3
# d = 50e-6


for liq in liqs:
    drop = Droplet(volatileName=liq, gasName="air")
    drop.set_P(P)
    drop.set_T(300.0)
    print(liq)
    print("Tsat:", drop.volatile.Tsat)
    drop.set_d(d)
    drop.set_mean_property_model("cantera")
    drop.set_kinetic_model("constreynolds", param=0.0)
    if model == "diffusion-only":
        drop.set_evaporation_model("ranzmarshall", None, "equil", "dilute")
    if model == "rapid-mixing":
        drop.set_evaporation_model("ranzmarshall", None, "equil")
    elif model == "Birds":
        drop.set_evaporation_model("ranzmarshall", "birds", "equil")
    elif model == "Abramzon-Sirignano":
        drop.set_evaporation_model("ranzmarshall", "absir", "equil")
    elif model == "Langmuir-Knudsen":
        drop.set_evaporation_model("ranzmarshall", "birds", "noneq")
    drop.set_temporal_scheme("implicit")

    fig = plt.figure()
    ax = []
    ax.append(fig.add_subplot(4, 1, 1))
    ax.append(fig.add_subplot(4, 1, 2))
    ax.append(fig.add_subplot(4, 1, 3))
    ax.append(fig.add_subplot(4, 1, 4))

    #
    # Mixture inlet fractions:
    #
    Ysvec = [0, 0.2, 0.4, 0.6, 0.8]
    # Ysvec = [0.4]

    npoin = 200

    Twb = np.empty([npoin])
    Yfwb = np.empty([npoin])
    BM = np.empty([npoin])
    err = np.empty([npoin])

    for Ys in Ysvec:
        #
        # Set seen vapur mass fraction
        #
        drop.set_Yvapseen(Ys)

        #
        # Start temperature from spot where relative humidity is 100%
        #
        Xs = y2x(Ys, drop.volatile.W, drop.bathgas.W)
        if Ys > 1e-6:
            Tmin = min(drop.volatile.Tc, drop.volatile.Tp2Psat.evalInv(P * Xs))
        else:
            Tmin = 288.15
        print("Ys={}, Tmin={}".format(Ys, Tmin))
        T = np.linspace(Tmin, 2000, npoin)
        for i, Ti in enumerate(T):
            drop.set_Tseen(Ti)
            drop.set_to_psychro_wet_bulb()

            Twb[i] = drop.T
            Yfwb[i] = drop.Yeq
            BM[i] = drop.BMeq
            err[i] = drop.psych_wet_bulb_error
            # err[i] = drop.psych_wet_bulb_steps

        ax[0].plot(T, Twb, label="$Y_{{f,s}}={}$".format(Ys))
        ax[1].plot(T, Yfwb)
        ax[2].plot(T, BM)
        ax[3].plot(T, err)

    ax[0].legend(
        loc="lower right",
        prop={"size": 12},
        framealpha=0.0,
        frameon=False,
        shadow=False,
        ncol=3,
    )  # , bbox_to_anchor=(1, 1.2))   #
    ax[-1].set_xlabel("$T_s$")
    ax[0].set_ylabel("$T_{wb}^{th}$")
    ax[1].set_ylabel("$Y_{f,wb}^{th}$")
    ax[2].set_ylabel("$B_M$")
    ax[3].set_ylabel("$err$")

    plt.pause(0.1)

plt.show()
