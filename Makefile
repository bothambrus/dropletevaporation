init:
	pip3 install -r requirements.txt

checkstyle:
	black --check dropletevaporation 
	black --check tests
	black --check examples
	isort --profile black --diff dropletevaporation
	isort --profile black --diff tests
	isort --profile black --diff examples

style:
	isort --profile black dropletevaporation
	black dropletevaporation 
	black tests
	black examples

test:
	rm -rf tests/tmp_testing/
	pytest --junitxml=junit_report.xml --cov=dropletevaporation --cov-report=html tests
	coverage xml
	mkdir -p dist 
	rm -rf dist/htmlcov
	mv coverage.xml dist/
	mv htmlcov dist/
	echo open dist/htmlcov/index.html to review code coverage
	python .cicd_scripts/test_coverage.py 70 dist/coverage.xml



.PHONY: init test


