#################################################################################
#   DropletEvaporation                                                          #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy

import numpy as np

from dropletevaporation.nasapoly import T2hcp
from dropletevaporation.property import SimpleGas, TwoPhaseSystem, y2x


def evaluateBM(Yvapseen, Yvapinterf):
    """
    Calculate the Spalding mass transfer number.
    """
    return max(
        -1 + 1e-6,
        min(1e6, (Yvapinterf - Yvapseen) / (1 - min(1.0 - 1e-12, Yvapinterf))),
    )


class PhaseChange(TwoPhaseSystem):
    """
    Class to evaluate interaction between two phases.
    """

    def __init__(self, Tseen=None, Yvapseen=None, volatileName="water", gasName="air"):
        #
        # Initialize parent class
        #
        TwoPhaseSystem.__init__(self, volatileName=volatileName, gasName=gasName)

        self.Yvapseen = None

        self.seen = SimpleGas()
        if not Yvapseen is None:
            self.set_Yvapseen(Yvapseen)
        if not Tseen is None:
            self.set_Tseen(Tseen)

        self.BMeq = None

    def __calculateEquilibriumBM__(self):
        """
        Calculate equilibrium mass transfer number.
        """
        if (not self.Yvapseen is None) and (not self.Yeq is None):
            self.BMeq = evaluateBM(self.Yvapseen, self.Yeq)

    def __calculateMaximumBM__(self):
        """
        Caculate highest possible mass transfer number
        on given pressure.
        """
        if (not self.Yvapseen is None) and (not self.Yeq_max is None):
            self.BMeq_max = evaluateBM(self.Yvapseen, self.Yeq_max)

    def set_P(self, P):
        """
        Overload and add BM calculation
        """
        TwoPhaseSystem.set_P(self, P)
        self.__calculateMaximumBM__()
        self.__calculateEquilibriumBM__()

    def set_T(self, T):
        """
        Overload and add BM calculation
        """
        TwoPhaseSystem.set_T(self, T)
        self.__calculateEquilibriumBM__()

    def set_Xeq(self, X):
        """
        Overload and add BM calculation
        """
        TwoPhaseSystem.set_Xeq(self, X)
        self.__calculateEquilibriumBM__()

    def set_Yeq(self, Y):
        """
        Overload and add BM calculation
        """
        TwoPhaseSystem.set_Yeq(self, Y)
        self.__calculateEquilibriumBM__()

    def set_Tseen(self, Tseen):
        """
        Set seen temperature
        """
        self.seen.set_T(Tseen)

    def set_Yvapseen(self, Yvapseen):
        """
        Set seen vapur mass fraction
        """
        #
        # Get mixture properties
        #
        W = 1 / (Yvapseen / self.volatile.W + (1 - Yvapseen) / self.bathgas.W)
        cpcoe = Yvapseen * self.volatile.cpcoe + (1 - Yvapseen) * self.bathgas.cpcoe
        X = {}
        X[self.volatile.formula] = y2x(Yvapseen, self.volatile.W, self.bathgas.W)
        for k in self.bathgas.X.keys():
            X[k] = self.bathgas.X[k] * (1 - X[self.volatile.formula])

        self.seen.set_mix(W, cpcoe, X)
        self.Yvapseen = Yvapseen
        self.__calculateMaximumBM__()
        self.__calculateEquilibriumBM__()

    def set_BMeq(self, BMeq, limit=True):
        """
        Set equilibrium Spalding mass transfer number.
        """
        BMeq = max(-1 + 1e-9, BMeq)
        if limit:
            BMeq = min(self.BMeq_max, max(-1 + 1e-9, BMeq))

        if not self.Yvapseen is None:
            Yvapinterf = (BMeq + self.Yvapseen) / (1 + BMeq)
            self.set_Yeq(Yvapinterf)

    def set_to_therm_wet_bulb(self):
        """
        Set interface state to thermodynamic wet-bulb conditions.
        """
        if np.isnan(self.T):
            self.set_T(self.seen.T)
        #
        # Initial guess
        #
        hsTs = self.seen.h
        if self.T == self.seen.T:
            self.set_T(self.seen.T - 1)
        hsTi, cpsTi = T2hcp(self.seen.cpcoe, self.T)
        BM = (hsTs - hsTi) / self.volatile.Lv
        self.set_BMeq(BM)

        #
        # Initialize ariables of iteravite solution
        #
        underrelax = 1.0
        tol = 1e-7
        error = 1
        dBM = 1e12
        errorPrev = error
        BMPrev = BM
        i = 0

        #
        # Iterate
        #
        while i < 1000 and abs(error) > tol:
            i += 1

            if i % 50 == 0:
                underrelax *= 0.9

            #
            # Calculate error
            #
            hsTi, cpsTi = T2hcp(self.seen.cpcoe, self.T)
            error = hsTs - hsTi - self.BMeq * self.volatile.Lv

            #
            # Derivative of error
            #
            if abs(dBM) < 1e-1:
                derr_dBM = (error - errorPrev) / (BM - BMPrev)
            else:
                derr_dBM = -1.0 * self.volatile.Lv

            #
            # Newton-Rhapson's to evaluate wet-bulb BM
            #
            if abs(derr_dBM) > 1e-12:
                dBM = -1.0 * underrelax * error / derr_dBM
            else:
                #
                # Leave previous step
                #
                pass

            #
            # Cheange BM
            #
            BMPrev = copy.deepcopy(self.BMeq)
            errorPrev = error

            BM = self.BMeq + dBM
            self.set_BMeq(BM, limit=False)

            #
            # If impossible, quit and set everything to nans
            #
            if self.BMeq > self.BMeq_max:
                if error > 0:
                    self.BMeq = np.nan
                    self.Yeq = np.nan
                    self.T = np.nan
                    break

        #
        # Evaluate final error
        #
        hsTi, cpsTi = T2hcp(self.seen.cpcoe, self.T)
        self.therm_wet_bulb_error = hsTs - hsTi - self.BMeq * self.volatile.Lv
