#################################################################################
#   DropletEvaporation                                                          #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy

import cantera as ct
import numpy as np

from dropletevaporation.interface import PhaseChange, evaluateBM
from dropletevaporation.nasapoly import T2hcp
from dropletevaporation.property import x2y


def d2m(d, rho):
    return rho * d ** 3 * np.pi / 6.0


def m2d(m, rho):
    return (m / rho * 6.0 / np.pi) ** (1.0 / 3.0)


def birds_func(b):
    return np.where(b < 1e-10, 1 / (1 + 0.5 * b), b / (np.exp(b) - 1))


def inv_abramzon_thickness_ratio(b):
    b = np.maximum(np.minimum(b, np.ones_like(b) * 20.0), np.zeros_like(b))
    return np.where(b < 1e-10, 1 - 0.2 * b, b / ((1 + b) ** 0.7 * np.log(1 + b)))


class Droplet(PhaseChange):
    """
    Class to evaluate different droplet states.
    """

    def __init__(self, d=None, volatileName="water", gasName="air"):
        #
        # Initialize parent class
        #
        PhaseChange.__init__(self, volatileName=volatileName, gasName=gasName)
        self.set_d(d)
        self.refprops = {}
        self.property_model = None
        self.property_weight = 0.33333333
        self.kinetic_model = None
        self.evaporation_model = None
        self.interface_mass_frac_model = None
        self.mass_transfer_potential = None
        self.heat_trans_corr = None
        self.temporal_scheme = "implicit"
        self.BMeq = None
        self.beta_eq = None
        self.BMneq = None
        self.beta_neq = None

        self.__update_mean_property__()
        self.__update_kinetic__()
        self.__update_evaporation__()

    def set_d(self, d):
        """
        Set droplet diameter.
        """
        self.d = d
        if not self.T is None:
            #
            # Calculate mass from diameter
            #
            self.m = d2m(self.d, self.volatile.rhop)

    def set_m(self, m):
        """
        Set droplet mass.
        """
        self.m = m
        if not self.T is None:
            #
            # Calculate diameter from mass
            #
            self.d = m2d(self.m, self.volatile.rhop)

    def set_kinetic_model(self, model_name, param):
        """
        Set kinetic model:
        constveloc:     velocity        = param
        constreynolds:  Reynolds number = param
        """
        #
        # Set model
        #
        self.kinetic_model = model_name

        if self.kinetic_model == "constveloc":
            self.veloc = param
        elif self.kinetic_model == "constreynolds":
            self.Re = param

        #
        # Calculate other parameters
        #
        self.__update_kinetic__()

    def __update_kinetic__(self):
        """
        Calculate Reynolds number and velocity of droplet.
        """
        if "nu" in self.refprops.keys():
            #
            # Calculate velocity from Reynolds or the other way around
            #
            if self.kinetic_model == "constveloc":
                self.Re = self.veloc * self.d / self.refprops["nu"]
            elif self.kinetic_model == "constreynolds":
                self.veloc = self.Re * self.refprops["nu"] / self.d

    def set_evaporation_model(
        self,
        model_name,
        heat_trans_corr=None,
        interface_mass_frac_model="equil",
        mass_transfer_potential="quasisteady",
    ):
        """
        Set all options of evaporation models.
        Models: Nu,Sh = f(Re)
            - Ranz-Marshall:                        "ranzmarshall"
        Corrections:
            - None:                                 None
            - Bird's:                               "birds"
            - Abramzon-Sirignano                    "absir"
        Interface mass fraction:
            - equilibrium:                          "equil"
            - non-equilibrium:                      "noneq"
            - no-itertion non-equilibrium:          "noneq1step"
        Mass transfer potential:
            - quasi-steady:                         "quasisteady"
            - dilute:                               "dilute"
        """
        #
        # Set evaporation model
        #
        self.evaporation_model = model_name
        self.heat_trans_corr = heat_trans_corr
        self.interface_mass_frac_model = interface_mass_frac_model
        self.mass_transfer_potential = mass_transfer_potential

        #
        # Update evaporation properties
        #
        self.__update_evaporation__()

    def __update_evaporation__(self):
        """
        Calculate Nusselt and Sherwood number .
        """
        #
        # Calculate Nusselt number without corrections
        #
        self.Nu0 = np.nan
        if "Pr" in self.refprops.keys():
            if self.evaporation_model == "ranzmarshall":
                self.Nu0 = (
                    2.0 + 0.6 * self.Re ** 0.5 * self.refprops["Pr"] ** 0.333333333333
                )

        #
        # Calculate Sherwood number without corrections
        #
        self.Sh0 = np.nan
        if "Sc" in self.refprops.keys():
            if self.evaporation_model == "ranzmarshall":
                self.Sh0 = (
                    2.0 + 0.6 * self.Re ** 0.5 * self.refprops["Sc"] ** 0.333333333333
                )

        #
        # Calculate for equilibrium interface vapour mass fraction:
        # beta_eq: The ratio of enthalpy transported by Stefan flow
        #          and the enthalpy that would be transported in
        #          the abcence of Stefan flow
        # Phi_m:   The factors of beta_eq that are independent of the
        #          Splading mass transfer number (BMeq)
        #
        self.beta_eq = np.nan
        if all(k in self.refprops.keys() for k in ["cpvap", "cp", "Le"]):
            self.Phi_m = (
                self.refprops["cpvap"]
                / self.refprops["cp"]
                * self.Sh0
                / (self.Nu0 * self.refprops["Le"])
            )
            if not (self.BMeq is None):
                self.beta_eq = self.Phi_m * np.log(1 + self.BMeq)

        #
        # Set surface to non-equilibrium state
        #
        if (
            not (self.interface_mass_frac_model is None)
            and "noneq" in self.interface_mass_frac_model
            and not (self.BMeq is None)
        ):
            #
            # Set 1-step case:
            # Non-equilibrium conditions based on
            # equilibrium transfer rates.
            #
            errorPrev = self.non_eq_calculations(self.BMeq)

            #
            # Iterative approach:
            #
            if not "1step" in self.interface_mass_frac_model:
                #
                # Initialize variables
                #
                tol = 1e-7
                error = 1
                i = 0
                underrelax = 1.0
                BMprev = copy.deepcopy(self.BMneq)

                #
                # Iterate
                #
                while i < 500 and abs(error) > tol:
                    i += 1
                    if i % 50 == 0:
                        underrelax *= 0.9

                    #
                    # Set new BM:
                    #
                    self.non_eq_calculations(self.BMneq)
                    error = self.BMneq - BMprev
                    self.BMneq = BMprev + underrelax * (self.BMneq - BMprev)
                    BMprev = copy.deepcopy(self.BMneq)

        if self.heat_trans_corr is None:
            #
            # No corrections
            #
            self.Sh = self.Sh0
            self.Nu = self.Nu0

        elif self.heat_trans_corr == "birds":
            #
            # Bird's correction (Correction of heat transfer for Stefan flow)
            #
            self.Sh = self.Sh0
            beta = np.nan
            if "noneq" in self.interface_mass_frac_model and not (
                self.beta_neq is None
            ):
                beta = self.beta_neq
            elif not (self.beta_eq is None):
                beta = self.beta_eq

            self.Nu = self.Nu0 * birds_func(beta)

        elif self.heat_trans_corr == "absir":
            #
            # Abramzon and Sirignano
            #
            BM = np.nan
            beta = np.nan
            if "noneq" in self.interface_mass_frac_model and not (self.BMneq is None):
                BM = self.BMneq
                beta = self.beta_neq
            elif not (self.BMeq is None):
                BM = self.BMeq
                beta = self.beta_eq

            invFM = inv_abramzon_thickness_ratio(BM)
            self.Sh = 2.0 + invFM * (self.Sh0 - 2.0)

            #
            # Iterative solution of heat transfer correction
            #
            ias = 0
            Nu_old = 1e10
            self.Nu = self.Nu0
            while (np.abs(Nu_old - self.Nu) / self.Nu > 1e-7) and (ias <= 100):
                Nu_old = copy.deepcopy(self.Nu)
                Phistar = (
                    self.refprops["cpvap"]
                    / self.refprops["cp"]
                    * self.Sh
                    / (self.Nu * self.refprops["Le"])
                )
                BT = (1 + BM) ** Phistar - 1
                invFT = inv_abramzon_thickness_ratio(BT)
                self.Nu = 2.0 + invFT * (self.Nu0 - 2.0)

                ias += 1

            self.Nu = self.Nu * birds_func(beta)

    def non_eq_calculations(self, BM1):
        #
        # Conditions at initial BM guess
        #
        beta1 = self.Phi_m * np.log(1 + BM1)

        #
        # Non-equilibrium surface vapour mole fraction
        #
        if "LK" in self.refprops.keys():
            self.Xneq = max(
                0.0 + 1e-18,
                min(1.0 - 1e-18, self.Xeq - 2.0 * self.refprops["LK"] / self.d * beta1),
            )
            self.Yneq = x2y(self.Xneq, self.volatile.W, self.bathgas.W)
            self.BMneq = evaluateBM(self.Yvapseen, self.Yneq)
            self.beta_neq = self.Phi_m * np.log(1 + self.BMneq)

            error = self.BMneq - BM1

            return error

    def set_mean_property_model(self, model_name, weight=0.333333333):
        """
        Set method to calculate mean properties that are used in evaluating the transport rates.
        Models:
            - Cantera:      "cantera"
        Weight:
            - For "1/3-law" type mean state
        """
        #
        # Set model
        #
        self.property_model = model_name
        self.property_weight = weight

        #
        # Calculate properties
        #
        self.__update_mean_property__()

    def __update_mean_property__(self):
        """
        Calculate refprop elements based on property model.
        """
        propertiesSet = False
        #
        # Apply weighting:
        #
        if not (self.Yvapseen is None) and not self.seen is None:
            Yvap_m = (
                self.property_weight * self.Yvapseen
                + (1 - self.property_weight) * self.Yeq
            )
            Yvap_m = min(1.0, max(0.0, Yvap_m))
            T_m = (
                self.property_weight * self.seen.T + (1 - self.property_weight) * self.T
            )
            T_m = min(5000.0, max(200.0, T_m))

            if self.property_model == "cantera":
                #
                # Set mean state
                #
                fuel_index = self.gasobject.species_index(self.volatile.formula)

                Y_m = {}
                Y_m[self.volatile.formula] = Yvap_m
                for k in self.bathgas.X.keys():
                    ik = self.gasobject.species_index(k)
                    Yk = (
                        self.gasobject.molecular_weights[ik]
                        / self.bathgas.W
                        * self.bathgas.X[k]
                    )
                    Y_m[k] = Yk * (1 - Yvap_m)

                self.gasobject.TPY = T_m, self.P, Y_m

                #
                # Get properties
                #
                self.refprops["rho"] = self.gasobject.density
                self.refprops["Df"] = self.gasobject.mix_diff_coeffs_mass[fuel_index]
                self.refprops["mu"] = self.gasobject.viscosity
                self.refprops["k"] = self.gasobject.thermal_conductivity
                self.refprops["cp"] = self.gasobject.cp_mass

                propertiesSet = True

            #
            # Vapour properties
            #
            hvap_m, cpvap_m = T2hcp(self.volatile.cpcoe, T_m)

        #
        # If not calculated
        #
        if not propertiesSet:
            self.refprops["rho"] = np.nan
            self.refprops["Df"] = np.nan
            self.refprops["mu"] = np.nan
            self.refprops["k"] = np.nan
            self.refprops["cp"] = np.nan
            cpvap_m = np.nan

        #
        # Calculate derived properties
        #
        self.refprops["Dt"] = self.refprops["k"] / (
            self.refprops["rho"] * self.refprops["cp"]
        )
        self.refprops["nu"] = self.refprops["mu"] / self.refprops["rho"]
        self.refprops["Pr"] = self.refprops["nu"] / self.refprops["Dt"]
        self.refprops["Sc"] = self.refprops["nu"] / self.refprops["Df"]
        self.refprops["Le"] = self.refprops["Dt"] / self.refprops["Df"]

        self.refprops["cpvap"] = cpvap_m

        if not (self.P is None) and not (self.T is None):
            self.refprops["LK"] = (
                self.refprops["rho"]
                * self.refprops["Df"]
                / self.P
                * np.sqrt(2 * np.pi * ct.gas_constant / self.volatile.W * self.T)
            )

    def get_mass_transfer_potential(self):
        """
        Return potential according to:
        Interface mass fraction:
            - equilibrium:                          "equil"
            - non-equilibrium:                      "noneq"
            - no-itertion non-equilibrium:          "noneq1step"
        Mass transfer potential:
            - quasi-steady:                         "quasisteady"
            - dilute:                               "dilute"
        """
        potential = None
        if "noneq" in self.interface_mass_frac_model:
            #
            # Non-equilibrium versions
            #
            if self.mass_transfer_potential == "quasisteady":
                potential = np.log(1 + self.BMneq)
            elif self.mass_transfer_potential == "dilute":
                potential = self.Yneq - self.Yvapseen

        else:
            #
            # Equilibrium versions
            #
            if self.mass_transfer_potential == "quasisteady":
                potential = np.log(1 + self.BMeq)
            elif self.mass_transfer_potential == "dilute":
                potential = self.Yeq - self.Yvapseen

        return potential

    def residual(self):
        """
        Calculate residual of current state.
        """
        #
        # Update all quantities
        #
        self.__update_mean_property__()
        self.__update_kinetic__()
        self.__update_evaporation__()

        self.residual_names = []
        self.unk = []
        self.res = []

        #################
        # Mass residual #
        #################
        self.residual_names.append("mass")
        potential = self.get_mass_transfer_potential()
        self.res.append(
            -1.0
            * np.pi
            * self.d
            * self.refprops["rho"]
            * self.refprops["Df"]
            * self.Sh
            * potential
        )
        self.unk.append(copy.deepcopy(self.m))

        #
        # Evaporation constant = d(d^2)/dt
        #
        self.evap_const = (
            -1.0 * self.res[-1] / (np.pi * 0.25 * self.volatile.rhop * self.d)
        )

        #################
        # Heat residual #
        #################
        self.residual_names.append("temperature")
        self.conv_heat_flow = (
            np.pi * self.d * self.refprops["k"] * self.Nu * (self.seen.T - self.T)
        )
        self.res.append(
            (self.conv_heat_flow + self.res[0] * self.volatile.Tp2Lv.eval(self.T))
            / (max(1e-18, self.m) * self.volatile.Tp2cpp.eval(self.T))
        )
        self.unk.append(copy.deepcopy(self.T))

        #
        # Numpy
        #
        self.unk = np.array(self.unk)
        self.res = np.array(self.res)

        #
        # Check if qualifies as wet-bulb condition
        #
        self.get_psych_wet_bulb_error()

    def jacobian(self):
        """
        Calculate Jacobian of droplet.
        """
        #
        # Get baseline values
        #
        self.residual()
        n_unk = len(self.res)
        res0 = np.array(copy.deepcopy(self.res))

        #
        # Save initial unknowns
        #
        u0 = np.zeros_like(self.res)
        for i, name in enumerate(self.residual_names):
            if name == "mass":
                u0[i] = copy.deepcopy(self.m)
            elif name == "temperature":
                u0[i] = copy.deepcopy(self.T)

        #
        # Create jacobian
        #
        self.jac = np.zeros([n_unk, n_unk])

        #
        # Evaluate Jacobian
        #
        for i, name in enumerate(self.residual_names):
            #
            # Decide increment and set new unknown
            #
            if name == "mass":
                dui = max(1e-18, 0.001 * self.m)
                self.set_m(u0[i] + dui)

            elif name == "temperature":
                if self.T > 200:
                    dui = -0.0001 * self.T
                else:
                    dui = 0.0001 * self.T
                self.set_T(u0[i] + dui)

            #
            # Reset previous unknown
            #
            if i > 0:
                if self.residual_names[i - 1] == "mass":
                    self.set_m(u0[i - 1])
                elif self.residual_names[i - 1] == "temperature":
                    self.set_T(u0[i - 1])

            #
            # Evaluate residual and Jacobian
            #
            self.residual()
            self.jac[:, i] = copy.deepcopy((self.res - res0) / dui)

        #
        # Reset to original
        #
        if self.residual_names[-1] == "mass":
            self.set_m(u0[-1])
        elif self.residual_names[-1] == "temperature":
            self.set_T(u0[-1])
        self.residual()

        #
        # If close to wet-bulb, zero down the Jacobian
        #
        if self.is_wet_bulb:
            self.jac = 0.0

    def set_temporal_scheme(self, scheme_name):
        """
        Set temporal scheme:
            - Euler forward:                            "explicit"
            - Euler backward with Jacobian evaluation:  "implicit"
        """
        #
        # Set model
        #
        self.temporal_scheme = scheme_name

    def advance(self, delta_t=None, substeps=True, scalestep=1.0):
        """
        Advance unknowns by delta_t time.
        """
        #
        # Get Jacobian
        #
        self.jacobian()
        n_unk = len(self.res)

        if substeps or delta_t is None:
            #
            # Estimate a time step size, and use substeps
            #
            if self.temporal_scheme == "implicit":
                step_scale = 0.05
            elif self.temporal_scheme == "explicit":
                step_scale = 0.005

            #
            # Limit time step more, for near-boiling conditions
            #
            for isafe in range(1, 5):
                if self.BMeq > 10 ** isafe:
                    step_scale *= 0.25
            #
            # Limit time step for supercritical cases
            #
            if self.Yeq_max < 0.95 and abs(self.Yeq - self.Yeq_max) < 0.1:
                step_scale *= 0.25

            #
            # Guess time step based on mass
            #
            dt_mass = np.inf
            dt_temp = np.inf
            if "mass" in self.residual_names:
                dt_mass = min(
                    step_scale
                    * self.m
                    / (np.abs(self.res[self.residual_names.index("mass")])),
                    1e2,
                )
            if "temperature" in self.residual_names:
                dt_temp = min(
                    step_scale
                    * 10
                    / (np.abs(self.res[self.residual_names.index("temperature")])),
                    1e2,
                )

                if self.mass_transfer_potential == "dilute" and (self.BMeq > 20):
                    #
                    # The temperature step might not be relevant in the dilute limit
                    #
                    dt_temp = np.inf

            dt = min(dt_mass, dt_temp) * scalestep

            if delta_t is None:
                nt = 1
                delta_t = dt
            else:
                nt = int(np.ceil(delta_t / dt))
                dt = delta_t / float(nt)

        else:
            #
            # Always attempt in one step
            #
            dt = delta_t * scalestep
            nt = 1

        for i in range(nt):
            if self.temporal_scheme == "implicit":
                ############
                # IMPLICIT #
                ############
                #
                # Matrix
                #
                aa = -1.0 * copy.deepcopy(self.jac)
                aa += np.eye(n_unk) / dt

                #
                # RHS:
                #
                bb = copy.deepcopy(self.res)
                bb += np.inner(self.unk, aa)

                #
                # Invert aa
                #
                invaa = np.linalg.inv(aa)

                #
                # Advance
                #
                unknext = np.inner(bb, invaa)
                for i, name in enumerate(self.residual_names):
                    if name == "mass":
                        self.set_m(max(0.0, unknext[i]))
                    elif name == "temperature":
                        self.set_T(max(200.0, min(self.volatile.Tsat, unknext[i])))
                #
                # Evaluate new Jacobian
                #
                self.jacobian()

            elif self.temporal_scheme == "explicit":
                ############
                # EXPLICIT #
                ############
                #
                # Advance
                #
                unknext = self.unk + dt * self.res
                for i, name in enumerate(self.residual_names):
                    if name == "mass":
                        self.set_m(max(0.0, unknext[i]))
                    elif name == "temperature":
                        self.set_T(max(200.0, min(self.volatile.Tsat, unknext[i])))
                #
                # Evaluate new residual
                #
                self.residual()

        return delta_t, nt

    def get_psych_wet_bulb_error(self):
        """
        Calculate error associated with the vicinity of the psycrometric wet-bulb condition.
        """
        #
        # Get error
        #
        self.psych_wet_bulb_error = copy.deepcopy(
            self.res[self.residual_names.index("temperature")]
        )

        #
        # Get tolerance
        #
        tol = 10

        #
        # If the case is at super-critical pressure
        # make the tolerance depend on the
        # latent heat of evaporation
        #
        if 1.0 - self.Yeq_max > 1e-2:
            tol = 5e-9 * self.volatile.Tp2Lv.eval(self.T)

        self.is_wet_bulb = abs(self.psych_wet_bulb_error) <= tol

        return tol

    def set_to_psychro_wet_bulb(self, reject_high_error=True):
        """
        Set to psychometric wet bulb conditions.
        """
        if np.isnan(self.T):
            self.set_T(self.volatile.Tsat - 1.0)

        m0 = copy.deepcopy(self.m)
        d0 = copy.deepcopy(self.d)

        Told = 1e10
        tol = 1e-8
        i = 0

        #
        # Loop with keeping diameter constant
        #
        while abs(self.T - Told) / self.T > tol and i < 2000:
            i += 1
            #
            # Save temperature
            #
            Told = copy.deepcopy(self.T)

            #
            # Advance droplet
            #
            delta_t, nt = self.advance()

            #
            # Set back to original diameter
            #
            self.set_d(d0)

        self.psych_wet_bulb_steps = i
        #
        # Check if satisfies criteria
        #
        tol = self.get_psych_wet_bulb_error()

        if (not self.is_wet_bulb) and reject_high_error:
            self.BMeq = np.nan
            self.Yeq = np.nan
            self.T = np.nan

            print(
                "Final error in wet-bulb search: {} > {}".format(
                    abs(self.psych_wet_bulb_error), tol
                )
            )

        #
        # Reset to original mass
        #
        self.set_m(m0)
        self.residual()
