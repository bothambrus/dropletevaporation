#################################################################################
#   DropletEvaporation                                                          #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np


def T2hcp(cpcoeffs, T):
    """
    Evaluate enthalpy and specific heat at given temperature.
    """
    #
    # Clip temperarure
    #
    T = min(3000.0, max(200.0, T))

    #
    # Use low or high temperature poly
    #
    if T < 1000.0:
        cpc = cpcoeffs[:, 0]
    else:
        cpc = cpcoeffs[:, 1]

    #
    # Evaluate polynomial
    #
    cp = cpc[4]
    h = cpc[4] / 5.0
    for i in range(3, -1, -1):
        cp *= T
        h *= T
        cp += cpc[i]
        h += cpc[i] / float(i + 1)
    h *= T
    h += cpc[5]

    return h, cp


def h2Tcp(cpcoeffs, h, Tini=273.15, tol=1e-7):
    """
    Evaluate temperature and specific heat at given enthalpy,
    using the Newton-Rhapson's method
    """
    #
    # Initialize
    #
    T = Tini
    err = np.inf
    maxit = 100
    it = 0

    #
    # Loop
    #
    while abs(err / h) > tol:
        h_guess, cp_guess = T2hcp(cpcoeffs, T)
        err = h_guess - h
        T -= err / cp_guess
        T = min(5000.0, max(200.0, T))
        it += 1
        if it >= maxit:
            break

    h_fin, cp_fin = T2hcp(cpcoeffs, T)
    return T, cp_fin
