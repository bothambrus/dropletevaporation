#################################################################################
#   DropletEvaporation                                                          #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy
import os

import cantera as ct
import numpy as np

from dropletevaporation.interpolate import Interpolator
from dropletevaporation.nasapoly import T2hcp, h2Tcp


def x2y(x, wf, wb):
    """
    Mass fraction of volatile component from mole fraction.
    """
    return x / (x + (1 - x) * wb / wf)


def y2x(y, wf, wb):
    """
    Mole fraction of volatile component from mass fraction.
    """
    return y / (y + (1 - y) * wf / wb)


class SimpleGas:
    """
    Class for evaluating the properties of the bath gas.
    """

    def __init__(self, name="air"):
        self.T = None
        self.h = None
        if name == "air":
            self.W = 28.8487
            self.cpcoe = np.transpose(
                np.array(
                    [
                        [
                            9.80027581e02,
                            1.39145377e-01,
                            -3.06020284e-04,
                            6.98175695e-07,
                            -3.60195091e-10,
                            -2.98188943e05,
                        ],
                        [
                            8.65027691e02,
                            4.28585162e-01,
                            -1.75288492e-04,
                            3.56573120e-08,
                            -2.84807148e-12,
                            -2.77326162e05,
                        ],
                    ]
                )
            )
            self.X = {"N2": 0.79, "O2": 0.21}

        elif name == "N2":
            self.W = 28.0140005
            self.cpcoe = np.transpose(
                np.array(
                    [
                        [
                            9.79036438e02,
                            4.17960972e-01,
                            -1.17627124e-03,
                            1.67438304e-06,
                            -7.25624894e-10,
                            -3.02999750e05,
                        ],
                        [
                            8.68617065e02,
                            4.41626668e-01,
                            -1.68721832e-04,
                            2.99676799e-08,
                            -2.00437236e-12,
                            -2.73883312e05,
                        ],
                    ]
                )
            )
            self.X = {"N2": 1.0}

    def set_mix(self, W, cpcoe, X):
        """
        Set to a new mixture.
        """
        self.W = copy.deepcopy(W)
        self.cpcoe = copy.deepcopy(cpcoe)
        self.X = copy.deepcopy(X)
        #
        # Keep temperature
        #
        if not (self.T is None):
            self.h, self.cp = T2hcp(self.cpcoe, self.T)

    def set_T(self, T):
        """
        Evaluate properties at given temeperature.
        """
        self.T = T
        self.h, self.cp = T2hcp(self.cpcoe, T)

    def set_h(self, h):
        """
        Evaluate properties at given temeperature.
        """
        self.h = h
        self.T, self.cp = h2Tcp(self.cpcoe, h)


class Volatile:
    """
    Class for evaluating the properties of the volatile component
    in the liquid and vapour phase.
    """

    def __init__(self, name="water"):
        #
        # Substance-specific properties
        #
        if name == "water":
            self.property_file = os.path.abspath(
                os.path.join(
                    os.path.dirname(__file__), "..", "data", "prop-H2O-prop.pts.res"
                )
            )
            self.formula = "H2O"
            self.W = 18.015
            self.Tc = 647.13
            self.cpcoe = np.transpose(
                np.array(
                    [
                        [
                            1.93776826e03,
                            -9.39860726e-01,
                            3.00931411e-03,
                            -2.53282345e-06,
                            8.17808290e-10,
                            -1.39812449e07,
                        ],
                        [
                            1.40025665e03,
                            1.00469726e00,
                            -7.57232046e-05,
                            -4.47870876e-08,
                            7.76285894e-12,
                            -1.38476666e07,
                        ],
                    ]
                )
            )
        elif name == "n-heptane":
            self.property_file = os.path.abspath(
                os.path.join(
                    os.path.dirname(__file__), "..", "data", "prop-NHEPT-prop.pts.res"
                )
            )
            self.formula = "NC7H16"
            self.W = 100.20592
            self.Tc = 540.2
            self.cpcoe = np.transpose(
                np.array(
                    [
                        [
                            -1.05111467e02,
                            7.08834543e00,
                            -4.35881888e-03,
                            1.35216873e-06,
                            -1.68028075e-10,
                            -2.12904380e06,
                        ],
                        [
                            1.84318494e03,
                            2.88459767e00,
                            -9.82402805e-04,
                            1.52080047e-07,
                            -8.80551408e-12,
                            -2.84398373e06,
                        ],
                    ]
                )
            )

        elif name == "n-decane":
            self.property_file = os.path.abspath(
                os.path.join(
                    os.path.dirname(__file__), "..", "data", "prop-NDECA-prop.pts.res"
                )
            )
            self.formula = "NC10H22"
            self.W = 142.285
            self.Tc = 617.7
            self.cpcoe = np.transpose(
                np.array(
                    [
                        [
                            2.80671037e01,
                            6.08229368e00,
                            -1.86222108e-03,
                            -1.31826467e-06,
                            8.42811922e-10,
                            -2.02260252e06,
                        ],
                        [
                            1.04250278e03,
                            3.88622605e00,
                            -1.36055427e-03,
                            2.20335451e-07,
                            -1.36510054e-11,
                            -2.31957642e06,
                        ],
                    ]
                )
            )

        elif name == "n-dodecane":
            self.property_file = os.path.abspath(
                os.path.join(
                    os.path.dirname(__file__), "..", "data", "prop-NDODE-prop.pts.res"
                )
            )
            self.formula = "NC12H26"
            self.W = 170.338
            self.Tc = 658.0
            self.cpcoe = np.transpose(
                np.array(
                    [
                        [
                            -1.27974574e02,
                            7.18688258e00,
                            -4.60765347e-03,
                            1.50066466e-06,
                            -1.97004002e-10,
                            -1.95565053e06,
                        ],
                        [
                            1.87970378e03,
                            2.75076813e00,
                            -9.34705609e-04,
                            1.44493955e-07,
                            -8.35867110e-12,
                            -2.67898320e06,
                        ],
                    ]
                )
            )

        elif name == "OME1":
            self.property_file = os.path.abspath(
                os.path.join(
                    os.path.dirname(__file__), "..", "data", "prop-OME1-prop.pts.res"
                )
            )
            self.formula = "CH3OCH2OCH3"
            self.W = 76.095
            self.Tc = 480.6
            self.cpcoe = np.transpose(
                np.array(
                    [
                        [
                            5.88794766e02,
                            1.51047205e00,
                            6.88226646e-03,
                            -1.06989650e-05,
                            4.51139348e-09,
                            -4.80276281e06,
                        ],
                        [
                            3.96520335e02,
                            3.95511836e00,
                            -1.98366712e-03,
                            4.68060949e-07,
                            -4.20707795e-11,
                            -4.75856398e06,
                        ],
                    ]
                )
            )

        #
        # Common treatment
        # Load data given by Alya (T,rhop,cpp,Psat,Lv)
        #
        data = np.loadtxt(self.property_file)

        #
        # Make interpolators
        #
        self.Tp2rhop = Interpolator(data[:, 0], data[:, 1])
        self.Tp2cpp = Interpolator(data[:, 0], data[:, 2])
        self.Tp2Psat = Interpolator(data[:, 0], data[:, 3])
        self.Tp2Lv = Interpolator(data[:, 0], data[:, 4])

        #
        # Critical pressure
        #
        self.Pc = self.Tp2Psat.eval(self.Tc)

    def set_P(self, P):
        """
        Set pressure and evaluate boiling point.
        """
        self.P = P
        self.Tsat = min(self.Tc, self.Tp2Psat.evalInv(P))

    def set_T(self, T):
        """
        Evaluate properties at given temeperature.
        """
        self.T = T
        self.hvap, self.cpvap = T2hcp(self.cpcoe, T)

        self.rhop = self.Tp2rhop.eval(T)
        self.cpp = self.Tp2cpp.eval(T)
        self.Psat = self.Tp2Psat.eval(T)
        self.Lv = self.Tp2Lv.eval(T)


class TwoPhaseSystem:
    """
    A system of volatile and bath gas component.
    """

    def __init__(self, volatileName="water", gasName="air"):
        self.volatile = Volatile(name=volatileName)
        self.bathgas = SimpleGas(name=gasName)
        self.P = None
        self.T = None
        self.Xeq = None
        self.Yeq = None
        self.gasobject = ct.Solution(
            os.path.join(os.path.dirname(__file__), "..", "data", "mech.cti")
        )

    def set_P(self, P):
        """
        Set pressue of system.
        """
        self.P = P
        self.volatile.set_P(P)

        self.Xeq_max = min(1.0 - 1e-18, self.volatile.Pc / self.P)
        self.Yeq_max = x2y(self.Xeq_max, self.volatile.W, self.bathgas.W)

        if not self.T is None:
            self.__calculateEquilibriumMixture__()

    def set_T(self, T):
        """
        Set temperature of system.
        """
        self.T = T
        self.volatile.set_T(T)
        self.bathgas.set_T(T)

        if not self.P is None:
            self.__calculateEquilibriumMixture__()

    def __calculateEquilibriumMixture__(self):
        """
        Calculate equilibrium gas mixture.
        """
        self.Xeq = self.volatile.Psat / self.P
        self.Yeq = x2y(self.Xeq, self.volatile.W, self.bathgas.W)

    def set_Xeq(self, X):
        """
        Set equilibrium vapour mole fraction at constant pressure.
        """
        T = self.volatile.Tp2Psat.evalInv(X * self.P)
        self.set_T(T)

    def set_Yeq(self, Y):
        """
        Set equilibrium vapour mass fraction at constant pressure.
        """
        X = y2x(Y, self.volatile.W, self.bathgas.W)
        self.set_Xeq(X)
