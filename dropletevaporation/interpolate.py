#################################################################################
#   DropletEvaporation                                                          #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy

import numpy as np


class Interpolator:
    """
    Takes two vectors as input.
    """

    def __init__(self, x, y):
        correctOrderX = np.argsort(x)
        self.x = copy.deepcopy(np.array(x)[correctOrderX])
        self.y = copy.deepcopy(np.array(y)[correctOrderX])

        correctOrderY = np.argsort(y)
        self.xinv = copy.deepcopy(np.array(x)[correctOrderY])
        self.yinv = copy.deepcopy(np.array(y)[correctOrderY])

    def eval(self, x):
        """
        Interpolates according to the first vector.
        """
        return np.interp(x, self.x, self.y)

    def evalInv(self, y):
        """
        Interpolates according to the second vector.
        """
        return np.interp(y, self.yinv, self.xinv)
