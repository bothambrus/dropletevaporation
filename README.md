# DropletEvaporation

Small python tool to track the evaporation of droplets using different calculation strategies.

For details on installation and usage, see the [wiki](https://gitlab.com/bothambrus/dropletevaporation/-/wikis/home).
